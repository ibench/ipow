# ipow() benchmark

Java 8 and Go (thanks to its assembly code) are the winners of non-GMP backends.

Perl 6 and Rust perform very badly.

Haskell and Julia both performs great thanks to their built-in GMP, but Julia FFI suffers overhead for "little" numbers.

CPU: Core i5 @ 2.5 GHz

## 2^100000

| Language | Time in seconds |
|----------|-------------:|
| Go | 0.05 |
| Haskell | 0.09 |
| Java 7 | 0.3 |
| Java 8 | 0.2 |
| Julia | 0.5 |
| Perl 6 | 1.1 |
| Python 3 | 0.2 |
| Ruby | 0.2 |
| Rust | 1 |

## 2^1000000

| Language | Time in seconds |
|----------|-------------:|
| Go | 0.4 |
| Haskell | 0.1 |
| Java 7 | 12 |
| Java 8 | 0.8 |
| Julia | 0.5 |
| Perl 6 | 100 |
| Python 3 | 2.2 |
| Ruby | 1.1 |
| Rust | 70 |

## 2^10000000

| Language | Time in seconds |
|----------|-------------:|
| Go | 27 |
| Java 8 | 7 |
| Haskell | 0.9 |
| Julia | 2 |

## 2^100000000

| Language | Time in seconds |
|----------|-------------:|
| Java 8 | 200 |
| Haskell | 20 |
| Julia | 12 |

## 2^1000000000

| Language | Time in seconds |
|----------|-------------:|
| Haskell | 222 |
| Julia | 171 |

## 2^10000000000

**Be careful**: this test requires 16 Go of RAM and 3 Go of storage by output.

CPU: Xeon @ 3.7 GHz

| Language | Time in seconds |
|----------|-------------:|
| Haskell | 1680 |
| Julia | 1310 |
