-- Copyright (C) 2016, Florent Gallaire <fgallaire@gmail.com>
-- MIT license

import System.Environment

main = do
       [argv0, argv1] <- getArgs
       let x = read argv0 :: Integer
       let n = read argv1 :: Integer
       print(x ^ n)
