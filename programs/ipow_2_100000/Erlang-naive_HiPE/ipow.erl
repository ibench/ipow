% Copyright (C) 2016, Florent Gallaire <fgallaire@gmail.com>
% MIT license

-module(ipow).
-export([main/1]).

ipow(X, 0) when is_integer(X) -> 1;
ipow(X, N) when is_integer(X), is_integer(N) > 0 -> X * ipow(X, N - 1).

main([String1, String2]) ->
    X = list_to_integer(String1),
    N = list_to_integer(String2),
    Result = ipow(X, N),
    io:format("~w\n", [Result]),
    erlang:halt(0).
