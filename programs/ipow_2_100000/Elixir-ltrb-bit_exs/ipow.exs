# Copyright (C) 2016, Florent Gallaire <fgallaire@gmail.com>
# MIT license

defmodule IPow do
    def ipow(x, 0) when is_integer(x) do
        1
    end
    def ipow(x, n) when is_integer(x) and is_integer(n) and n > 4294967295 do
        :infinity
    end
    def ipow(x, n) when is_integer(x) and is_integer(n) and n > 0 do
        ipow(x, <<n :: size(32)>>, 1)
    end
    def ipow(_x, <<>>, acc) do
        acc
    end
    def ipow(x, <<0 :: size(1), tail :: bitstring>>, acc) do
        ipow(x, tail, acc * acc)
    end
    def ipow(x, <<1 :: size(1), tail :: bitstring>>, acc) do
        ipow(x, tail, acc * acc * x)
    end
end

[argv0, argv1] = System.argv()

x = String.to_integer(argv0)
n = String.to_integer(argv1)

IO.puts IPow.ipow(x, n)
