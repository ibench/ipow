# Copyright (C) 2016, Florent Gallaire <fgallaire@gmail.com>
# MIT license

x = Integer(ARGV[0])
n = Integer(ARGV[1])

puts x ** n
