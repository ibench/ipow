# Copyright (C) 2016, Florent Gallaire <fgallaire@gmail.com>
# MIT license

x = parse(BigInt, ARGS[1])
n = parse(BigInt, ARGS[2])

println(x ^ n)
