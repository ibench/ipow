// Copyright (C) 2016, Florent Gallaire <fgallaire@gmail.com>
// MIT license

package main

import "os"
import "fmt"
import "math/big"

func ipow(x *big.Int, n *big.Int) *big.Int {
    var and big.Int 
    acc := big.NewInt(1)
    zero := big.NewInt(0)
    one := big.NewInt(1)
    for n.Cmp(zero) == 1 {
        if one.Cmp(and.And(n, one)) == 0 {
            acc.Mul(acc, x)
        }
        x.Mul(x, x)
        n.Rsh(n, 1)
    }
    return acc
}

func main() {
    x := big.NewInt(0)
    x.SetString(os.Args[1], 10)
    n := big.NewInt(0)
    n.SetString(os.Args[2], 10)
    fmt.Println(ipow(x, n))
}
