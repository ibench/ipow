# Copyright (C) 2016, Florent Gallaire <fgallaire@gmail.com>
# MIT license

from sys import argv

def ipow(x, n):
    acc = 1
    while n > 0:
        if n & 1:
            acc = acc * x
        x = x * x
        n = n >> 1
    return acc

x = int(argv[1])
n = int(argv[2])

print(ipow(x, n))
