% Copyright (C) 2016, Florent Gallaire <fgallaire@gmail.com>
% MIT license

ipow(X, 0) when is_integer(X) -> 1;
ipow(X, N) when is_integer(X), is_integer(N), N > 0 -> ipow(X, N, 1).
ipow(_X, 0, Acc) -> Acc;
ipow(X, N, Acc) -> ipow(X, N - 1, X * Acc).

main([String1, String2]) ->
    X = list_to_integer(String1),
    N = list_to_integer(String2),
    Result = ipow(X, N),
    io:format("~w\n", [Result]).
