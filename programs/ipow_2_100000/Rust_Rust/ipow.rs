// Copyright (C) 2017, Cédric Cabessa
// MIT license

extern crate num;

use std::env;
use std::str::FromStr;
use num::bigint::BigInt;
use num::pow;

fn main() {
    let args: Vec<String> = env::args().collect();
    let x = BigInt::from_str(&args[1]).unwrap();
    let n : usize = args[2].parse().unwrap();
    println!("{}", pow(x, n));
}
