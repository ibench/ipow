// Copyright (C) 2016, Florent Gallaire <fgallaire@gmail.com>
// MIT license

package main

import "os"
import "fmt"
import "math/big"

func main() {
    x := big.NewInt(0)
    x.SetString(os.Args[1], 10)
    n := big.NewInt(0)
    n.SetString(os.Args[2], 10)
    result := big.NewInt(0)
    result.Exp(x, n, nil)
    fmt.Println(result)
}
