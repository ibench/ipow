% Copyright (C) 2016, Florent Gallaire <fgallaire@gmail.com>
% Copyright Ericsson AB 1996-2016. All Rights Reserved.
% Licensed under the Apache License, Version 2.0

ipow(X, 0) when is_integer(X) -> 1;
ipow(X, N) when is_integer(X), is_integer(N), N > 0 -> ipow(X, N, 1).
ipow(X, N, R) when N < 2 -> R * X;
ipow(X, N, R) -> ipow(X * X, N bsr 1, case N band 1 of 1 -> R * X; 0 -> R end).

main([String1, String2]) ->
    X = list_to_integer(String1),
    N = list_to_integer(String2),
    Result = ipow(X, N),
    io:format("~w\n", [Result]).
