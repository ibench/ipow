# Copyright (C) 2016, Florent Gallaire <fgallaire@gmail.com>
# MIT license

defmodule IPow do
    def ipow(x, 0) when is_integer(x) do
        1
    end
    def ipow(x, n) when is_integer(x) and is_integer(n) and n > 0 do
        x * ipow(x, n - 1)
    end
end

[argv0, argv1] = System.argv()

x = String.to_integer(argv0)
n = String.to_integer(argv1)

IO.puts IPow.ipow(x, n)
