# Copyright (C) 2016, Florent Gallaire <fgallaire@gmail.com>
# MIT license

from sys import argv

x = int(argv[1])
n = int(argv[2])

print(x ** n)
