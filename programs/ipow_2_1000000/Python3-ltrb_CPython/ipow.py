# Copyright (C) 2016, Florent Gallaire <fgallaire@gmail.com>
# MIT license

from sys import argv

def ipow(x, n):
    bits = bin(n)[2:]
    acc = 1
    for bit in bits:
        if bit == '0':
            acc = acc * acc
        elif bit == '1':
            acc = acc * acc * x
    return acc

x = int(argv[1])
n = int(argv[2])

print(ipow(x, n))
