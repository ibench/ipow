// Copyright (C) 2016, Florent Gallaire <fgallaire@gmail.com>
// MIT license

package main

import "os"
import "fmt"
import "strconv"
import "math/big"

func ipow(x *big.Int, n int) *big.Int {
    bits := strconv.FormatInt(int64(n), 2) 
    acc := big.NewInt(1)
    for _, bit := range bits {
        acc.Mul(acc, acc)
        if bit == 49 {
            acc.Mul(acc, x)
        }
    }
    return acc
}

func main() {
    x := big.NewInt(0)
    x.SetString(os.Args[1], 10)
    n, _ := strconv.Atoi(os.Args[2])
    fmt.Println(ipow(x, n))
}
