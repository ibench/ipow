# Copyright (C) 2016, Florent Gallaire <fgallaire@gmail.com>
# MIT license

use Bitwise

defmodule IPow do
    def ipow(x, 0) when is_integer(x) do
        1
    end
    def ipow(x, n) when is_integer(x) and is_integer(n) and n > 0 do
        ipow(x, n, 1)
    end
    def ipow(x, n, acc) when n < 2 do
        acc * x
    end
    def ipow(x, n, acc) do
        ipow(x * x, bsr(n, 1), case band(n, 1) do 1 -> acc * x; 0 -> acc end)
    end
end

[argv0, argv1] = System.argv()

x = String.to_integer(argv0)
n = String.to_integer(argv1)

IO.puts IPow.ipow(x, n)
