// Copyright (C) 2016, Florent Gallaire <fgallaire@gmail.com>
// MIT license

import java.math.BigInteger;

public class ipow {
   public static void main(String args[]) { 
      BigInteger x = new BigInteger(args[0]);
      int n = Integer.parseInt(args[1]);
      System.out.println(x.pow(n));
   }               
}
