# Copyright (C) 2016, Florent Gallaire <fgallaire@gmail.com>
# MIT license

my $x = @*ARGS[0];
my $n = @*ARGS[1];

say($x ** $n);
