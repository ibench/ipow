% Copyright (C) 2016, Florent Gallaire <fgallaire@gmail.com>
% Copyright Ericsson AB 1996-2016. All Rights Reserved.
% Licensed under the Apache License, Version 2.0

-module(ipow).
-export([main/1]).

ipow(X, 0) when is_integer(X) -> 1;
ipow(X, N) when is_integer(X), is_integer(N), N > 0 -> ipow(X, integer_to_list(N, 2), 1).
ipow(_X, [], Acc) -> Acc;
ipow(X, [48|Tail], Acc) -> ipow(X, Tail, Acc * Acc);
ipow(X, [49|Tail], Acc) -> ipow(X, Tail, Acc * Acc * X).

main([String1, String2]) ->
    X = list_to_integer(String1),
    N = list_to_integer(String2),
    Result = ipow(X, N),
    io:format("~w\n", [Result]),
    erlang:halt(0).
