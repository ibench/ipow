# Copyright (C) 2016, Florent Gallaire <fgallaire@gmail.com>
# MIT license

from sys import argv

def ipow(x, bits, acc=1):
    if bits:
        if bits[0] == '0':
            return ipow(x, bits[1:], acc * acc)
        elif bits[0] == '1':
            return ipow(x, bits[1:], acc * acc * x)
    return acc

x = int(argv[1])
n = int(argv[2])

print(ipow(x, bin(n)[2:]))
